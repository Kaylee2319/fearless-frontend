import React from "react";
import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
    <div className="container-fluid">
      <ul id="navbarSupportedContent" className="navbar-nav me-auto mb-2">
        <li className="nav-item">
        <NavLink aria-current="page" className="nav-link" to="/">
          Home
        </NavLink>
        </li>
        <li className="nav-item">
          <NavLink aria-current="page" className="nav-link" to="/locations/new">
            New location
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink aria-current="page" className="nav-link" to="/conferences/new">
            New conference
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink aria-current="page" className="nav-link" to="/attendees">
            Attendees
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink aria-current="page" className="nav-link" to="/attendees/new">
            New Attendees
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink aria-current="page" className="nav-link" to="/presentations/new">
            New presentation
          </NavLink>
        </li>
      </ul>
    </div>
    </nav>

  );
}

export default Nav;

import React from "react";
import { BrowserRouter, Routes, Route, Outlet } from "react-router-dom";
import Nav from "./components/Nav";
import ConferenceForm from "./components/conferenceForm";
import AttendConferenceForm from "./components/AttendConferenceForm";
import LocationForm from "./components/LocationForm";
import AttendeesList from "./components/AttendeesList";
import NewPresentation from "./components/newPresentation";
import MainPage from "./components/MainPage";
import EditConferencePage from "./components/EditConference"

function App() {

  return (
    <BrowserRouter>
      <Nav />
      <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="locations/new" element={<LocationForm />} />
          <Route path="conferences/new" element={<ConferenceForm />} />
          <Route path="attendees" element={<AttendeesList />} />
          <Route path="attendees/new" element={<AttendConferenceForm />} />
          <Route path="presentations/new" element={<NewPresentation />} />
          <Route path="/edit/conference/:id" element={<EditConferencePage />} />
      </Routes>
    </BrowserRouter>
  );
}


export default App;


{/* <BrowserRouter>
<Nav />
<Routes>
  <Route index element={<MainPage />} />
  <Route path="locations">
    <Route path="new" element={<LocationForm />} />
  </Route>
  <Route path="conferences">
    <Route path="new" element={<ConferenceForm />} />
  </Route>
  <Route path="attendees">
    <Route index element={<AttendeesList attendees={props.attendees} />} />
    <Route path="new" element={<AttendConferenceForm />} />
  </Route>
  <Route path="presentations">
    <Route path="new" element={<NewPresentation />} />
  </Route>
</Routes>
</BrowserRouter>
);
} */}


//   return (
//     <BrowserRouter>
//       <Routes>
//         <Route path="/"
//           element={
//             <>
//               <Nav />
//               <div className="container">
//                 <Outlet />
//               </div>
//             </>
//           }
//         >
    //       <Route path="/" element={<MainPage />} />
    //       <Route path="locations/new" element={<LocationForm />} />
    //       <Route path="conferences/new" element={<ConferenceForm />} />
    //       <Route index element={<AttendeesList />} />
    //       <Route path="attendees/new" element={<AttendConferenceForm />} />
    //       <Route path="presentations/new" element={<NewPresentation />} />
    //     </Route>
    //   </Routes>
    // </BrowserRouter>
//   );
// }



// import React from "react";
// import { createBrowserRouter, RouterProvider, Outlet } from "react-router-dom";
// import Nav from "./components/Nav";
// import ConferenceForm from "./components/conferenceForm";
// import AttendConferenceForm from "./components/AttendConferenceForm";
// import LocationForm from "./components/LocationForm";
// import AttendeesList from "./components/AttendeesList";
// import NewPresentation from "./components/newPresentation";
// import MainPage from "./components/MainPage";

// function App(props) {
//   if (props.attendees === undefined) {
//     return null;
//   }

//   const router = createBrowserRouter([
//     {
//       path: "/",
//       element: (
//         <>
//           <Nav />
//           <div className="container">
//             <Outlet />
//           </div>
//         </>
//       ),
//       children: [
//         { path: "/", element: <MainPage /> },
//         {
//           path: "locations",
//           children: [
//             { path: "new", element: <LocationForm /> },
//           ],
//         },
//         { path: "conferences/new", element: <ConferenceForm /> },
//         { path: "attendees", element: <AttendeesList attendees={props.attendees} />
//         },
//         { path: "attendees/new", element: <AttendConferenceForm /> },
//         { path: "presentations/new", element: <NewPresentation /> },
//       ],
//     },
//   ]);

//   return <RouterProvider router={router} />;
// }

// export default App;

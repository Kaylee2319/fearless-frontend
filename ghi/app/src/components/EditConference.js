import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

const EditConferencePage = () => {
  const { id } = useParams();
  const [locations, setLocations] = useState([]);
  const [conference, setConference] = useState(null);
  const [name, setName] = useState('');
  const [starts, setStarts] = useState('');
  const [ends, setEnds] = useState('');
  const [description, setDescription] = useState('');
  const [maxPresentations, setMaxPresentations] = useState('');
  const [maxAttendees, setMaxAttendees] = useState('');
  const [location, setLocation] = useState('');

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }

  const handleStartsChange = (event) => {
    const value = event.target.value;
    setStarts(value);
  }

  const handleEndsChange = (event) => {
    const value = event.target.value;
    setEnds(value);
  }

  const handleDescriptionChange = (event) => {
    const value = event.target.value;
    setDescription(value);
  }

  const handleMaxPresentationsChange = (event) => {
    const value = event.target.value;
    setMaxPresentations(value);
  }

  const handleMaxAttendeesChange = (event) => {
    const value = event.target.value;
    setMaxAttendees(value);
  }

  const handleLocationChange = (event) => {
    const value = event.target.value;
    setLocation(value);
  }

  const fetchData = async () => {

    try {
      // Fetch locations
      const locationsUrl = 'http://localhost:8000/api/locations/';
      const locationsResponse = await fetch(locationsUrl);

      if (locationsResponse.ok) {
        const locationsData = await locationsResponse.json();
        setLocations(locationsData.locations || []);
      }

      // Fetch conference details
      const conferenceUrl = `http://localhost:8000/api/conferences/${id}/`;
      const conferenceResponse = await fetch(conferenceUrl);

      if (conferenceResponse.ok) {
        const conferenceData = await conferenceResponse.json();
        const formattedStartDate = new Date(conferenceData.conference.starts).toLocaleDateString("sv-SE");
        const formattedEndDate = new Date(conferenceData.conference.ends).toLocaleDateString("sv-SE");
        // Set conference data when available
        setConference(conferenceData);

        // Set form fields based on conference data
        setName(conferenceData.conference.name);
        setStarts(formattedStartDate);
        setEnds(formattedEndDate);
        setDescription(conferenceData.conference.description);
        setMaxPresentations(conferenceData.conference.max_presentations);
        setMaxAttendees(conferenceData.conference.max_attendees);

        // Check if location exists before setting state
        if (conferenceData.conference.location && conferenceData.conference.location.id) {
          setLocation(conferenceData.conference.location.id);
        }
      } else {
        console.error(conferenceResponse);
      }
    } catch (error) {
      console.error(error);
    }
  }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {
          name,
          starts,
          ends,
          description,
          max_presentations: maxPresentations,
          max_attendees: maxAttendees,
          location,
        };
        // console.log(data)

        const conferenceUrl = `http://localhost:8000/api/conferences/${id}/`;
        const fetchConfig = {
          method: "PUT",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
          const newConference = await response.json();


        //   setName('');
        //   setStarts(starts);
        //   setEnds(ends);
        //   setDescription(description);
        //   setMaxPresentations(maxPresentations);
        //   setMaxAttendees(maxAttendees);
        //   setLocation(location);
        }
      };


      useEffect(() => {
        fetchData();
      }, [id]);

    return (
        <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Edit Conference</h1>
                <form onSubmit={handleSubmit} id="create-conference-form">
                  <div className="form-floating mb-3">
                    <input
                        onChange={handleNameChange}
                        placeholder={name}
                        value={name || ''}
                        required
                        type="text"
                        id="name"
                        name="name"
                        className="form-control"/>
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input
                        onChange={handleStartsChange}
                        required
                        value={starts || ''}
                        type="date"
                        id="starts"
                        name="starts"
                        className="form-control"/>
                    <label htmlFor="starts">starts</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input
                        onChange={handleEndsChange}
                        required
                        value={ends || ''}
                        type="date"
                        id="ends"
                        name="ends"
                        className="form-control"/>
                    <label htmlFor="ends">ends</label>
                  </div>
                  <div className="form-floating mb-3">
                    <label htmlFor="description"></label>
                    <textarea
                        onChange={handleDescriptionChange}
                        value={description || ''}
                        id="description"
                        name="description"
                        rows="4"
                        cols="77"
                        placeholder="Enter your description here...">
                    </textarea>
                  </div>
                  <div className="form-floating mb-3">
                    <input
                        onChange={handleMaxPresentationsChange}
                        value={maxPresentations || ''}
                        placeholder="max presentations"
                        required
                        type="number"
                        id="max_presentations"
                        name="max_presentations"
                        className="form-control"/>
                    <label htmlFor="max_presentations">max presentations</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input
                        onChange={handleMaxAttendeesChange}
                        placeholder="max attendees"
                        value={maxAttendees || ''}
                        required
                        type="number"
                        id="max_attendees"
                        name="max_attendees"
                        className="form-control"/>
                    <label htmlFor="max_attendees">max attendees</label>
                  </div>
                  <div className="mb-3">
                    <select
                        value={location || ''}
                        onChange={handleLocationChange}
                        required id="location"
                        name="location"
                        className="form-select">
                      <option value="" >Choose a location</option>
                      {locations.map(location => (
                      <option key={location.id} value={location.id}>
                        {location.name}
                      </option>
                    ))}
                    </select>
                  </div>
                  <button className="btn btn-primary">Save Changes</button>
                </form>
              </div>
            </div>
          </div>
      );
    }

export default EditConferencePage;

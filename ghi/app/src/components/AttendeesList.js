import { useEffect, useState } from 'react';

function AttendeesList() {
  const [attendees, setAttendees] = useState([]);

  const getData = async () => {
    const response = await fetch('http://localhost:8001/api/attendees/');

    if (response.ok) {
      const data = await response.json();
      setAttendees(data.attendees);
    }
  }

  const handleDelete = async (id) => {
    try {
      const request = await fetch(`http://localhost:8001/api/attendees/${id}/`, {
        method: 'DELETE',
      });

      if (request.ok) {
        getData();
      } else {
        const errorData = await request.json();
        console.error('Failed to delete attendee:', errorData);
      }
    } catch (error) {
      console.error('Error during delete request:', error);
    }
  }


  useEffect(() => {
    getData();
  }, []);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Conference</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {attendees.map((attendee) => (
          <tr key={attendee.id}>
            <td>{attendee.name}</td>
            <td>{attendee.conference}</td>
            <td>
              <button
                onClick={() => {handleDelete(attendee.id)}}
                className='btn btn-danger'
              >
                Delete
              </button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export default AttendeesList;
